<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Task

Что нужно сделать
Для того чтобы оценить технические скиллы мы придумали простое приложение, в котором можно показать навыки
работы с теми технологиями, которые используются нами в продакшне. Node.js приложение должно реализовывать 2 endpoint:

- /track?id={tracker_id} где tracker_id - uuid, соответствующий полю uuid в Postgre таблице `trackers`.
  Если трекер с таким id найден - нужно отправить данные в CH (или подготовить к отправке туда) и вернуть ответ HTTP 204.
  Вместе с ответом нужно установить cookie user_id (сгенерировать новое значение, или обновить предыдущее).
- /stats?tracker_id={tracker_id}&from={from}&to={to} - должен возвращать кол-во событий по трекеру tracker_id из Clickhouse.
  from и to - даты для фильтрации событий. Этот эндпоинт будет вызываться крайне редко

Данные, которые нужно отправить в CH:
- date - дата события (UTC)
- date_time - дата и время события (UTC)
- event_id - уникальный id события. Должен быть сгенерирован при обработке трекинг события,
- tracker_id - id трекера,
- ip - ip адрес пользователя, запросившего трекер,
- user_id - user id пользователя, запросившего трекер. Должен определяться по cookie с именем user_id
- user_agent String - user agent браузера, запросившего трекер
- url String - полный url, по которому был запрошен трекер,
- value String - значение из postgre таблицы trackers

Несколько советов:
- endpoint /track должен быть реализован с учетом того, что он будет принимать большое кол-во трафика.
- Можно использовать любые доп сервисы, для реализации задачи. Например логично отправлять данные в Clickhouse используя Kafka
- По условию задачи данные в таблице `trackers` меняются очень редко и только добавляются (никогда не удаляются, но значение value может изредка обновляться). Приемлемо какое-то время использовать старое значение value

Время на выполнение:
Для человека, имеющего опыт работы со всеми компонентами выполнение займет 4-6 часов.
Не имея опыта предположительно займёт вдвое больше. Мы понимаем, что сложно выделить такое кол-во времени
одновременно, поэтому даём 1 неделю на выполнение. Еслии за неделю задание выполнить не удалось - лучше
отправить хотя бы частичный результат, чтобы было что посмотреть и объяснить в чём было затруднение

Что не нужно (не обязательно) делать в рамках этой задачи:
- миграции в PG / CH. Достаточно написать скрипт, который положит данные в Postgre, либо описать что за шаги надо
  выполнить перед проведением теста

Как оформить результат:
Предпочтительно оформить docker-compose файл, который поднимет все компоненты. если это невозможно - сделать описание
что именно нужно сделать, чтобы запустить приложение. Прислать результат можно как ссылку на git репозиторий, или как tar архив
Крайне желательно написать интеграционные тесты для приложения

Как будет проверяться задание:
Для начала мы проведем ревью кода. Если задание выполнено полностью - развернём код на linux сервере (Ubuntu 20.04).
Перед http сервисом будет развернут nginx в качестве reverse-proxy. Запустим небольшое кол-во трафика (в районе 100rps) и дадим
поработать в течении 10-15 минут. После этого проверим как отвечало приложение и проверим что за данные попали в
Clickhouse / как само приложение возвращает данные из CH.


## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
