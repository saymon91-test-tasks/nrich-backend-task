import { ConnectionOptions } from 'typeorm';

const config = {
  type: 'postgres',
  host: process.env.TYPEORM_HOST,
  port: process.env.TYPEORM_PORT,
  username: process.env.TYPEORM_USERNAME,
  password: process.env.TYPEORM_PASSWORD,
  database: process.env.TYPEORM_DATABASE,
  logging: process.env.TYPEORM_LOGGING,
  entities: ['./**/db/typeorm/entities/*.ts'],
  migrations: ['./**/db/typeorm/migrations/*.ts'],
  seeds: ['./**/db/typeorm/seeds/*.ts'],
  factories: ['./**/db/typeorm/seed-factories/*.ts'],
} as ConnectionOptions;
export = config;
