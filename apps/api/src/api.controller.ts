import {
  Body,
  Controller,
  Get,
  NotFoundException,
  ParseUUIDPipe,
  Patch,
  Query,
  Req,
  Res,
} from '@nestjs/common';
import {
  ApiConsumes,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiProduces,
  ApiTags,
} from '@nestjs/swagger';
import type { Request, Response } from 'express';
import { v4 } from 'uuid';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';

import { ApiService } from './api.service';
import { Cookies } from './cookie.decorator';

import type { GetTrackerRequest } from './types';
import { SetTrackerValueDto } from '@apps/api/src/set-tracker-value.dto';
import { retry } from '../../../libs/utils/retry';

@ApiTags('tracker')
@ApiConsumes('application/json')
@ApiProduces('application/json')
@Controller('/')
export class ApiController {
  constructor(private readonly apiService: ApiService) {}

  @ApiImplicitQuery({
    name: 'id',
    type: 'string',
    description: 'UUIDv4',
    example: '5d4629d4-e31b-4701-a021-7c48dca92c15',
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @Get('track')
  public async hasTracker(
    @Query('id', new ParseUUIDPipe({ version: '4', errorHttpStatusCode: 409 }))
    trackerId: string,
    @Req() { url, ip, useragent }: Request,
    @Res() res: Response,
    @Cookies('user_id') userId?: string,
  ): Promise<void> {
    if (!userId) {
      userId = v4();
    }

    const data: GetTrackerRequest = {
      trackerId,
      userId: userId as string,
      url,
      ip,
      userAgent: useragent?.browser ?? 'unknown',
    };

    // const exists = await retry<boolean>(
    //   () => this.apiService.hasTracker(data),
    //   {
    //     times: 3,
    //     interval: 1000,
    //   },
    // );

    const exists = await this.apiService.hasTracker(data)

    if (exists) {
      res.cookie('user_id', userId).status(204).send(null);
    } else {
      throw new NotFoundException({ trackerId }, 'Tracker is not found');
    }
  }

  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @Patch('track')
  public async getHello(
    @Body() { trackerId, value }: SetTrackerValueDto,
    // @Req() { url, ip, useragent }: Request,
    @Res() res: Response,
    @Cookies('user_id') userId?: string,
  ): Promise<void> {
    if (!userId) {
      userId = v4();
    }
    //
    // const data: GetTrackerRequest = {
    //   trackerId,
    //   userId: userId as string,
    //   url,
    //   ip,
    //   userAgent: useragent?.browser ?? 'unknown',
    // };
    // if (await this.apiService.hasTracker(data)) {
    //   res.cookie('user_id', userId).status(204).send(null);
    // } else {
    //   throw new NotFoundException({ trackerId }, 'Tracker is not found');
    // }

    await this.apiService.setTrackerValue(trackerId, value);
    res.cookie('user_id', userId).status(204).send(null);
  }
}
