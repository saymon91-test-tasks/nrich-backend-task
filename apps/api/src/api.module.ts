import { ClientsModule, Transport } from '@nestjs/microservices';
import { Logger, LoggerService, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { logLevel } from '@nestjs/microservices/external/kafka.interface';
import {
  WinstonModule,
  utilities,
  WINSTON_MODULE_NEST_PROVIDER,
  WinstonLogger,
} from 'nest-winston';
import { transports, format } from 'winston';
import { RedisModule } from 'nestjs-redis'

import { ApiController } from './api.controller';
import { ApiService } from './api.service';
import { TrackerEntity } from './db/typeorm/entities/tracker.entity';
import { TypeormWinstonLogger } from './typeorm-winston-logger';

import ormconfig = require('../ormconfig');

@Module({
  imports: [
    WinstonModule.forRoot({
      transports: [
        new transports.Console({
          level: process.env.LOG_LEVEL,
          format: format.combine(
            format.timestamp(),
            format.ms(),
            utilities.format.nestLike(),
            format.colorize(),
          ),
        }),
      ],
    }),
    RedisModule.register({
      url: process.env.REDIS_URL,
      name: 'cache',
      keyPrefix: 'api-cache:',
    }),
    ClientsModule.registerAsync([
      {
        name: 'events',
        imports: [WinstonModule],
        inject: [WINSTON_MODULE_NEST_PROVIDER],
        useFactory: (logger: WinstonLogger) => ({
          transport: Transport.KAFKA,
          options: {
            // serializer: {
            //   serialize(value: any): any {
            //     console.log(value)
            //     logger.debug && logger.debug(`Serialize data: ${JSON.stringify(value)}`);
            //     return { value: 'JSON.stringify(value)' };
            //   },
            // },
            client: {
              connectionTimeout: 1000,
              logLevel:
                process.env.LOG_LEVEL === 'debug'
                  ? logLevel.DEBUG
                  : logLevel.ERROR,
              clientId: 'api',
              brokers: (process.env.KAFKA_URLS as string).split(','),
            },
          },
        }),
      },
    ]),
    TypeOrmModule.forRootAsync({
      imports: [WinstonModule],
      inject: [WINSTON_MODULE_NEST_PROVIDER],
      useFactory: (logger: LoggerService) => ({
        ...ormconfig,
        // autoLoadEntities: true,
        cache: {
          type: 'ioredis',
          duration: process.env.CACHE_DURATION
            ? +process.env.CACHE_DURATION
            : 5000,
          options: process.env.REDIS_URL,
        },
        migrations: [],
        entities: [TrackerEntity],
        logger: new TypeormWinstonLogger(logger),
        extra: {
          max: 350,
          min: 20,
          idleTimeoutMillis: 30000,
          connectionTimeoutMillis: 5000,
        },
      }),
    }),
    TypeOrmModule.forFeature([TrackerEntity]),
  ],
  controllers: [ApiController],
  providers: [ApiService, Logger],
})
export class ApiModule {}
