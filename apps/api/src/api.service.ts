import {
  Inject,
  Injectable,
} from '@nestjs/common';
import type { OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { WINSTON_MODULE_NEST_PROVIDER, WinstonLogger } from 'nest-winston';
import { plainToClass } from 'class-transformer';
import { RedisService } from 'nestjs-redis';
import { Redis } from 'ioredis';

import { TrackerEntity } from './db/typeorm/entities/tracker.entity';
import type { GetTrackerEventMessage } from '@libs/common-types/events';
import { GetTrackerEventMessageDto } from '@libs/common-types/get-tracker-event-message.dto';
import type { GetTrackerRequest } from './types';

const { KAFKA_GET_TRACKER_EVENT } = process.env;
const CACHE_DURATION = process.env.CACHE_DURATION
  ? +process.env.CACHE_DURATION
  : 5000;

@Injectable()
export class ApiService implements OnModuleInit, OnModuleDestroy {
  private readonly redisCacheClient!: Redis;
  constructor(
    @InjectRepository(TrackerEntity)
    private readonly trackerRepo: Repository<TrackerEntity>,
    @Inject('events') private readonly eventsClient: ClientKafka,
    @Inject(WINSTON_MODULE_NEST_PROVIDER)
    private readonly logger: WinstonLogger,
    redisCache: RedisService,
  ) {
    this.redisCacheClient = redisCache.getClient('cache');
  }

  public async onModuleInit(): Promise<any> {
    await this.eventsClient.connect();
  }

  public async onModuleDestroy(): Promise<any> {
    await this.eventsClient.close();
  }

  private async getTrackerValueFromCache(
    trackerId: string,
  ): Promise<string | null> {
    const cachedValue = await this.redisCacheClient.get(trackerId);
    if (cachedValue) {
      this.redisCacheClient
        .pexpire(trackerId, CACHE_DURATION)
        .catch((err) => this.logger.error(err.message, err.stack));
    }
    return cachedValue;
  }

  private cacheTrackerValue(
    trackerId: string,
    value: string,
    timeout: number = CACHE_DURATION,
  ): string {
    this.redisCacheClient
      .psetex(trackerId, timeout, value)
      .catch((err) => this.logger.error(err.message, err.stack));
    return value;
  }

  private async getStoredTrackerValue(
    trackerId: string,
    cache = false,
  ): Promise<string | null> {
    const tracker = await this.trackerRepo.findOne(
      { uuid: trackerId },
      { select: ['value'] },
    );

    if (!tracker) {
      return null;
    }

    return cache
      ? this.cacheTrackerValue(trackerId, tracker.value, CACHE_DURATION)
      : tracker.value;
  }

  public async setTrackerValue(
    trackerId: string,
    value: string,
  ): Promise<void> {
    const { affected } = await this.trackerRepo.update(
      { uuid: trackerId },
      { value },
    );
    if (affected) {
      await this.cacheTrackerValue(trackerId, value, CACHE_DURATION);
    } else {
      throw new Error(`Tracker ${trackerId} is not found`);
    }
  }

  public async hasTracker({
    trackerId,
    ...eventData
  }: GetTrackerRequest): Promise<boolean> {
    const value =
      (await this.getTrackerValueFromCache(trackerId)) ??
      (await this.getStoredTrackerValue(trackerId));
    if (!value) {
      return false;
    }

    const event: GetTrackerEventMessage = plainToClass(
      GetTrackerEventMessageDto,
      {
        ...eventData,
        trackerId,
        value,
      },
    );
    this.logger.debug && this.logger.debug(event, 'send get-tracker event');
    this.eventsClient.emit(KAFKA_GET_TRACKER_EVENT, {
      key: trackerId,
      value: { event },
    });

    return true;
  }
}
