import type { Tracker } from '../../../types';
import {
  Column,
  Entity,
  Generated,
  Index,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Index('trackers_uuid_idx', ['uuid'], { unique: true })
@Entity({ name: 'trackers' })
export class TrackerEntity implements Tracker {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  readonly id!: number;

  @Column('uuid')
  @Generated('uuid')
  readonly uuid!: string;

  @Column({ type: 'character varying', length: 32 })
  readonly value!: string;
}
