import type { MigrationInterface, QueryRunner } from 'typeorm';
import { Table } from 'typeorm';
import { TrackerEntity } from '@apps/api/src/db/typeorm/entities/tracker.entity';

export class CreateTableTrackers1626859615795 implements MigrationInterface {
  async up(queryRunner: QueryRunner): Promise<void> {
    const trackersRepo = queryRunner.manager.getRepository(TrackerEntity);
    const trackersTable = Table.create(
      trackersRepo.metadata,
      queryRunner.connection.driver,
    );
    await queryRunner.createTable(trackersTable, true);
  }

  async down(queryRunner: QueryRunner): Promise<void> {
    const trackersRepo = queryRunner.manager.getRepository(TrackerEntity);
    const trackersTable = Table.create(
      trackersRepo.metadata,
      queryRunner.connection.driver,
    );
    await queryRunner.dropTable(trackersTable);
  }
}
