import { define } from 'typeorm-seeding';
import Faker from 'faker';

import { TrackerEntity } from '../entities/tracker.entity';

define(TrackerEntity, (faker: typeof Faker) => {
  const tracker = new TrackerEntity();
  const value = faker.random.words(1);
  return Object.assign(tracker, {
    value: value.slice(0, Math.min(value.length, 31)),
  });
});
