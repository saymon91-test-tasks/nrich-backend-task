import type { Factory, Seeder } from 'typeorm-seeding';
import type { Connection } from 'typeorm';

import { TrackerEntity } from '../entities/tracker.entity';

const TRACKERS_COUNT = 1000;
const BULK_COUNT = 500;

export default class CreateTrackers implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const em = connection.getRepository(TrackerEntity);
    let currentCount = 0;
    while (TRACKERS_COUNT > (currentCount = await em.count())) {
      console.log('Trackers in DB', currentCount);
      await factory(TrackerEntity)().createMany(
        Math.min(BULK_COUNT, TRACKERS_COUNT - currentCount),
      );
    }
  }
}
