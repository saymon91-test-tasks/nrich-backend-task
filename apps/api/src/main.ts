// eslint-disable-next-line @typescript-eslint/no-var-requires
require('../../../libs/utils/config')('api');
import { NestFactory } from '@nestjs/core';
import { utilities, WinstonModule } from 'nest-winston';
import { transports, format } from 'winston';
import * as cookieParser from 'cookie-parser';
import * as userAgent from 'express-useragent';

import { ApiModule } from './api.module';
import { AllExceptionFilter } from './all-exception.filter';

const DEFAULT_NODE_PORT = 3000;

async function bootstrap() {
  const logger = WinstonModule.createLogger({
    transports: [
      new transports.Console({
        level: process.env.LOG_LEVEL,
        format: format.combine(
          format.timestamp(),
          format.ms(),
          utilities.format.nestLike(),
          format.colorize(),
        ),
      }),
    ],
  });
  const app = await NestFactory.create(ApiModule, { logger });
  app.use(cookieParser());
  app.use(userAgent.express());
  app.useGlobalFilters(new AllExceptionFilter(logger));
  await app.listen(process.env.PORT ? +process.env.PORT : DEFAULT_NODE_PORT);
}
bootstrap();
