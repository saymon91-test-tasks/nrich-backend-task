import { IsDefined, IsString, IsUUID, MaxLength } from 'class-validator';

import { SetTrackerValue } from '@apps/api/src/types';

export class SetTrackerValueDto implements SetTrackerValue {
  @IsDefined()
  @IsUUID(4)
  trackerId!: string;

  @IsDefined()
  @IsString()
  @MaxLength(32)
  value!: string;
}
