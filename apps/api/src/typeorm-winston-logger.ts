import { Logger } from 'typeorm';
import { LoggerService } from '@nestjs/common';

const logLevels: Record<
  'log' | 'info' | 'warn',
  Extract<keyof LoggerService, 'log' | 'warn'>
> = {
  log: 'log',
  info: 'log',
  warn: 'warn',
};

export class TypeormWinstonLogger implements Logger {
  constructor(private readonly logger: LoggerService) {}

  log(level: 'log' | 'info' | 'warn', message: any): any {
    return this.logger[logLevels[level]](message);
  }

  logMigration(message: string): any {
    this.logger.log(message, 'MIGRATION');
  }

  logQuery(query: string, parameters?: any[]): any {
    return (
      this.logger.debug && this.logger.debug({ query, parameters }, 'QUERY')
    );
  }

  logQueryError(error: string | Error, query: string, parameters?: any[]): any {
    const [message, stack] =
      error instanceof Error ? [error.message, error.stack] : [error, ''];
    this.logger.error({ message, query, parameters }, stack, 'QUERY-ERROR');
  }

  logQuerySlow(time: number, query: string, parameters?: any[]): any {
    this.logger.warn({ time, query, parameters }, 'QUERY-SLOW');
  }

  logSchemaBuild(message: string): any {
    this.logger.log(message, 'SCHEMA_BUILD');
  }
}
