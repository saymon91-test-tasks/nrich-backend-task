import { GetTrackerEventMessage } from '@libs/common-types/events';

export interface Tracker {
  id: number;
  uuid: string;
  value: string;
}

export type SetTrackerValue = Pick<Tracker, 'value'> & { trackerId: string };
export type GetTrackerRequest = Omit<GetTrackerEventMessage, 'value'>;
