// eslint-disable-next-line @typescript-eslint/no-var-requires
require('../../../libs/utils/config')('load-test');
import { createLogger, transports, format } from 'winston';
import { createConnection } from 'typeorm';
import { parallelLimit, times } from 'async';
import axios from 'axios';
import { Agent } from 'http';

import { TrackerEntity } from '@apps/api/src/db/typeorm/entities/tracker.entity';

const logger = createLogger({
  handleExceptions: true,
  transports: [
    new transports.Console({
      level: process.env.LOG_LEVEL,
      format: format.combine(
        format.timestamp(),
        format.cli(),
        format.colorize(),
      ),
    }),
  ],
});

async function bootstrap() {
  const axInstance = axios.create({ baseURL: process.env.URL });

  const connection = await createConnection({
    type: 'postgres',
    host: process.env.TYPEORM_HOST as string,
    port: process.env.TYPEORM_PORT ? +process.env.TYPEORM_PORT : 5432,
    username: process.env.TYPEORM_USERNAME as string,
    password: process.env.TYPEORM_PASSWORD as string,
    database: process.env.TYPEORM_DATABASE as string,
    entities: [TrackerEntity],
  });
  logger.info('Database connection established');

  const items = await connection.getRepository(TrackerEntity).find({
    select: ['uuid'],
    order: { uuid: 'DESC' },
    skip: 0,
    take: 1000,
  });

  let requests = 0;

  const requestTimes: number[] = [];
  let lastCheck = Date.now();

  const stat = () => {
    const [a, b] = requestTimes.reduce(
      ([top, bottom], reqTime, index) => [
        top + reqTime * ++index,
        bottom + ++index,
      ],
      [0, 0],
    );
    const checkTime = Date.now();
    logger.info(
      `Total requests: ${requests}. Average RPS: ${requestTimes.length / ((checkTime - lastCheck) / 1000)}, average request time: ${a / b} ms`,
    );
    requestTimes.length = 0;
    lastCheck = Date.now();
  };

  setInterval(() => stat(), 2000);

  logger.info('Start requests');
  await parallelLimit(
    items.map(
      ({ uuid }) =>
        (cb) =>
          times(
            10,
            async (index, cb) => {
              logger.debug(`Exec request ${uuid}:${index}`);
              const httpAgent = new Agent({ keepAlive: true });
              const start = Date.now();
              await axInstance
                .get('track', { params: { id: uuid }, httpAgent })
                .catch((err) => logger.error(err.message, err.stack));
              requests++;
              const end = Date.now();
              logger.debug(`Request duration: ${end - start}ms`);
              requestTimes.push(end - start);
              cb();
            },
            () => cb(),
          ),
    ),
    40,
  );
  stat();
  process.exit(0);
}
bootstrap().catch((err) => logger.error(err.message, err.stack));
