import { Exclude, Expose, Transform, Type } from 'class-transformer';
import {
  IsDate,
  IsDateString,
  IsDefined,
  IsIP,
  IsString,
  IsUrl,
  IsUUID,
  MaxLength,
} from 'class-validator';
import { DateTime } from 'luxon';

import { GetTrackerEventMessageDto } from '@libs/common-types/get-tracker-event-message.dto';
import type { GetTrackerEventStored } from './types';

export class GetTrackerEventStoredDto
  extends GetTrackerEventMessageDto
  implements GetTrackerEventStored
{
  @Expose({ toPlainOnly: true })
  get date(): string {
    return (
      this.dateTime instanceof Date
        ? DateTime.fromJSDate(this.dateTime)
        : DateTime.fromISO(this.dateTime)
    ).toFormat('yyyy-MM-dd');
  }

  @IsDate({ always: true })
  @IsDateString({}, { always: true })
  @Type(() => Date)
  @Expose({ name: 'date_time', toPlainOnly: true })
  @Transform(
    ({ value }) =>
      DateTime.fromJSDate(value).toFormat('yyyy-MM-dd HH:mm:s.SZZ'),
    { toPlainOnly: true },
  )
  dateTime!: string | Date;

  @IsUUID(4, { always: true })
  @Expose({ name: 'event_id', toPlainOnly: true })
  eventId!: string;
}

class PayloadValueDto {
  @Type(() => GetTrackerEventMessageDto)
  event!: GetTrackerEventStored;
}

export class PayloadDto {
  @Type(() => PayloadValueDto)
  value!: { event: GetTrackerEventStored };
}
