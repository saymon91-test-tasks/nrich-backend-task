import { Controller, Inject } from '@nestjs/common';
import { EventPattern, Payload } from '@nestjs/microservices';

import { LoggerService } from './logger.service';
import { PayloadDto } from './get-tracker-event-stored.dto';
import { WINSTON_MODULE_NEST_PROVIDER, WinstonLogger } from 'nest-winston';

const { KAFKA_GET_TRACKER_EVENT } = process.env;
@Controller()
export class LoggerController {
  constructor(
    @Inject(WINSTON_MODULE_NEST_PROVIDER)
    private readonly logger: WinstonLogger,
    private readonly loggerService: LoggerService,
  ) {}

  @EventPattern(KAFKA_GET_TRACKER_EVENT)
  async onGetTracker(@Payload() payload: PayloadDto): Promise<void> {
    this.logger.debug && this.logger.debug(payload, KAFKA_GET_TRACKER_EVENT);
    return this.loggerService.storeGetTrackerEvent(payload.value.event);
  }
}
