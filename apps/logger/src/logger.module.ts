import { Module } from '@nestjs/common';
import { utilities, WinstonModule } from 'nest-winston';
import { format, transports } from 'winston';
import { ScheduleModule } from '@nestjs/schedule';

import { LoggerController } from './logger.controller';
import { LoggerService } from './logger.service';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    WinstonModule.forRoot({
      transports: [
        new transports.Console({
          level: process.env.LOG_LEVEL,
          format: format.combine(
            format.timestamp(),
            format.ms(),
            utilities.format.nestLike(),
            format.colorize(),
          ),
        }),
      ],
    }),
  ],
  controllers: [LoggerController],
  providers: [LoggerService],
})
export class LoggerModule {}
