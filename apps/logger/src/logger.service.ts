import { Inject, Injectable } from '@nestjs/common';
import type { OnModuleInit } from '@nestjs/common';
import { WINSTON_MODULE_NEST_PROVIDER, WinstonLogger } from 'nest-winston';
import { classToPlain, plainToClass } from 'class-transformer';
import { v4 } from 'uuid';
import { SchedulerRegistry } from '@nestjs/schedule';
import { ClickHouse } from 'clickhouse';

import type { GetTrackerEventMessage } from '@libs/common-types/events';
import { GetTrackerEventMessageDto } from '@libs/common-types/get-tracker-event-message.dto';
import { GetTrackerEventStoredDto } from './get-tracker-event-stored.dto';
import type { GetTrackerEventStored } from './types';

const DEFAULT_BULK_SIZE = 1000;
const WRITE_TIMEOUT = process.env.CH_WRITE_TIMEOUT
  ? +process.env.CH_WRITE_TIMEOUT
  : 1000; // ms
const BULK_SIZE = process.env.BULK_SIZE
  ? +process.env.BULK_SIZE
  : DEFAULT_BULK_SIZE;
const WRITE_TIMEOUT_NAME = 'bulk-write';
const TABLE_NAME = 'tracking_events';

const chEventsFields = [
  'date',
  'date_time',
  'event_id',
  'tracker_id',
  'ip',
  'user_id',
  'user_agent',
  'url',
  'value',
];

@Injectable()
export class LoggerService implements OnModuleInit {
  private readonly client: ClickHouse = new ClickHouse({
    host: process.env.CH_HOST,
    post: process.env.CH_PORT,
    debug: process.env.LOG_LEVEL === 'debug',
    format: 'json',
    config: { database: process.env.CH_DATABASE },
  });
  private readonly queue: GetTrackerEventMessage[] = [];
  private writing = false;

  constructor(
    @Inject(WINSTON_MODULE_NEST_PROVIDER)
    private readonly logger: WinstonLogger,
    private readonly schedulerRegistry: SchedulerRegistry,
  ) {}

  public async onModuleInit() {
    await this.client
      .query(
        `
        CREATE TABLE IF NOT EXISTS ${TABLE_NAME} (
          date Date,
          date_time DateTime,
          event_id String,
          tracker_id String,
          ip String,
          user_id String,
          user_agent String,
          url String,
          value String
        ) ENGINE = MergeTree PARTITION BY toYYYYMM(date) ORDER BY (tracker_id) SETTINGS index_granularity = 8192;
`,
      )
      .toPromise();
    this.createWriteBulkTimer();
  }

  private createWriteBulkTimer(timeout: number = WRITE_TIMEOUT) {
    this.clearWriteBulkTimer();

    this.debugLog('Creating write timeout');
    this.schedulerRegistry.addTimeout(
      WRITE_TIMEOUT_NAME,
      setTimeout(() => {
        this.debugLog(`Timeout ${WRITE_TIMEOUT_NAME} is left`);
        this.writeBulk();
      }, timeout),
    );
  }

  private clearWriteBulkTimer() {
    if (!this.schedulerRegistry.doesExists('timeout', WRITE_TIMEOUT_NAME)) {
      return;
    }

    this.debugLog('Clearing write timeout');
    clearTimeout(this.schedulerRegistry.getTimeout(WRITE_TIMEOUT_NAME));
    this.schedulerRegistry.deleteTimeout(WRITE_TIMEOUT_NAME);
  }

  private isQueueFilled(): boolean {
    return this.queue.length >= BULK_SIZE;
  }

  private toBulk(event: GetTrackerEventStored): number {
    return this.queue.push(event);
  }

  private debugLog(message: any, context = 'EventsLogger'): any {
    return this.logger.debug && this.logger.debug(message, context);
  }

  private async writeBulk(): Promise<void> {
    if (this.writing) {
      return;
    }

    if (!this.queue.length) {
      return this.createWriteBulkTimer();
    }

    this.writing = true;
    this.debugLog(`Start writing ${this.queue.length} events`);
    this.debugLog('Data transformation');
    console.log(this.queue);
    const items: Record<string, any>[] = classToPlain(this.queue) as Record<
      string,
      any
    >[];
    this.debugLog('Clearing queue');
    this.queue.length = 0;
    this.debugLog('Creating CH stream');
    console.log(items);
    await this.client
      .insert(`INSERT INTO ${TABLE_NAME} (${chEventsFields.join(',')})`, items)
      .toPromise();
    this.debugLog(`CH stream ending`);
    this.debugLog(`Finish writing`);
    this.writing = false;
    this.createWriteBulkTimer();
  }

  public async storeGetTrackerEvent(
    data: GetTrackerEventMessageDto,
  ): Promise<void> {
    const now = new Date();

    const queueLength = this.toBulk(
      plainToClass(GetTrackerEventStoredDto, {
        ...data,
        eventId: v4(),
        dateTime: now,
      }),
    );
    this.debugLog(`${queueLength} events in queue`);
  }
}
