import { NestFactory } from '@nestjs/core';
import { utilities, WinstonModule } from 'nest-winston';
import { format, transports } from 'winston';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

import { LoggerModule } from './logger.module';
import { ValidationPipe } from '@nestjs/common';
import { logLevel } from '@nestjs/microservices/external/kafka.interface';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    LoggerModule,
    {
      logger: WinstonModule.createLogger({
        transports: [
          new transports.Console({
            level: process.env.LOG_LEVEL,
            format: format.combine(
              format.timestamp(),
              format.ms(),
              utilities.format.nestLike(),
              format.colorize(),
            ),
          }),
        ],
      }),
      transport: Transport.KAFKA,
      options: {
        // deserializer: {
        //   deserialize({ value, ...props }: any): any {
        //     return {
        //       ...props,
        //       value: JSON.parse(Buffer.from(value, 'base64').toString()),
        //     };
        //   },
        // },
        client: {
          logLevel:
            process.env.LOG_LEVEL === 'debug' ? logLevel.DEBUG : logLevel.ERROR,
          clientId: 'logger',
          brokers: (process.env.KAFKA_URLS as string).split(','),
        },
        consumer: {
          groupId: 'logger',
        },
      },
    },
  );
  //app.useGlobalPipes(new ValidationPipe());
  await app.listenAsync();
}
bootstrap();
