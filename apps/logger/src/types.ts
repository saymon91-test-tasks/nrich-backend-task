import { GetTrackerEventMessage } from '@libs/common-types/events';

export interface GetTrackerEventStored extends GetTrackerEventMessage {
  date: string | Date;
  dateTime: string | Date;
  eventId: string;
}
