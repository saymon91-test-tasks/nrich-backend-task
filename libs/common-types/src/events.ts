export interface GetTrackerEventMessage {
  trackerId: string;
  ip: string;
  userId: string;
  userAgent: string;
  url: string;
  value: string;
}
