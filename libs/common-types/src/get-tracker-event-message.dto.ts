import {
  IsDefined,
  IsIP,
  IsString,
  IsUrl,
  IsUUID,
  MaxLength,
} from 'class-validator';
import { Expose } from 'class-transformer';
import type { GetTrackerEventMessage } from '@libs/common-types/events';

export class GetTrackerEventMessageDto implements GetTrackerEventMessage {
  @IsDefined({ always: true })
  @IsIP(4, { always: true })
  ip!: string;

  @IsDefined({ always: true })
  @IsUUID(4, { always: true })
  @Expose({ name: 'tracker_id', toPlainOnly: true })
  trackerId!: string;

  @IsDefined({ always: true })
  @IsUrl({}, { always: true })
  url!: string;

  @IsDefined({ always: true })
  @IsString({ always: true })
  @Expose({ name: 'user_agent', toPlainOnly: true })
  userAgent!: string;

  @IsDefined({ always: true })
  @IsUUID(4, { always: true })
  @Expose({ name: 'user_id', toPlainOnly: true })
  userId!: string;

  @IsDefined({ always: true })
  @IsString({ always: true })
  @MaxLength(32)
  value!: string;
}
