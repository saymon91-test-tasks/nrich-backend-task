module.exports = (appName) => {
  if (
    !process.env.NODE_ENV ||
    process.env.NODE_ENV === 'dev' ||
    process.env.NODE_ENV === 'development'
  ) {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const { config } = require('dotenv');
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const { resolve } = require('path');

    config({ path: resolve(process.cwd(), `.${appName}.env`) });
  }
};
