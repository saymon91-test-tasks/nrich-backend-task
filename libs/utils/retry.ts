import { wait } from './wait';

export interface RetryOptions {
  times?: number;
  interval?: number;
}

// @ts-ignore
export const retry = async <T>(task: () => Promise<T>, options: RetryOptions = {}): Promise<T> => {
  let { times = 1 } = options;
  const { interval = 0 } = options;
  do {
    --times;
    try {
      return await task();
    } catch (err) {
      if (times > 0) {
        await wait(interval);
      } else {
        throw err;
      }
    }
  } while (times > 0);
};
