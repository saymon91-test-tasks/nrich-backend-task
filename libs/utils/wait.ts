export const wait = (timeout = 0) =>
  new Promise((res) => setTimeout(() => res, timeout));
